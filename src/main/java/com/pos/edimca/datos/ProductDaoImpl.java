/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pos.edimca.datos;

import com.pos.edimca.domain.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mauricio
 */
@Stateless 
public class ProductDaoImpl implements ProductDao{

    @PersistenceContext(unitName="ProductPU")
    EntityManager em;

    @Override
    public List<Product> findAllProducts() {
        return em.createNamedQuery("Product.findAll").getResultList();
    }

    @Override
    public Product findProductById(Product product) {
        return em.find(Product.class, product.getIdProduct());
    }

    @Override
    public Product findProductByName(Product product) {
        Query query = em.createQuery("from Product p where p.name =: name");
        query.setParameter("name", product.getName());
        return (Product) query.getSingleResult();
    }

    @Override
    public void insertProduct(Product product) {
        em.persist(product);
    }

    @Override
    public void updateProduct(Product product) {
        em.merge(product);
    }

    @Override
    public void deleteProduct(Product product) {
        em.remove(em.merge(product));
    }    
}
