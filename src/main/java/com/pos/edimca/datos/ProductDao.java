/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pos.edimca.datos;

import com.pos.edimca.domain.Product;
import java.util.List;

/**
 *
 * @author mauricio
 */
public interface ProductDao {
    public List<Product> findAllProducts();
    
    public Product findProductById(Product product);
    
    public Product findProductByName(Product product);
    
    public void insertProduct(Product product);
    
    public void updateProduct(Product product);
    
    public void deleteProduct(Product product);
    
}
