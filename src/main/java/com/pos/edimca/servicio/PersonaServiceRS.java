package com.pos.edimca.servicio;

import com.pos.edimca.datos.ProductDao;
import com.pos.edimca.domain.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;


@Stateless
@Path("/products")
public class PersonaServiceRS {
    
    @Inject
    private ProductDao productDao;
    
    @GET
    @Produces(value=MediaType.APPLICATION_JSON)
    public List<Product> listarProductos(){
        List<Product> product =  productDao.findAllProducts();
        System.out.println("productos encontrados:" + product);
        return product;
    }
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}") 
    public Product encontrarProducto(@PathParam("id") int id){
        Product product = productDao.findProductById(new Product(id));
        System.out.println("producto encontrado:" + product);
        return product;
    }
    
    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Product agregarProducto(Product product){
        productDao.insertProduct(product);
        System.out.println("product agregado:" + product);
        return product;
    }
    
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response modificarProducto(@PathParam("id") int id, Product updatedProduct){
        Product product = productDao.findProductById(new Product(id));
        if(product != null){
            productDao.updateProduct(updatedProduct);
            System.out.println("producto modificado:" + updatedProduct);
            return Response.ok().entity(updatedProduct).build();
        }
        else{
            return Response.status(Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response eliminarProducto(@PathParam("id") int id){
        productDao.deleteProduct(new Product(id));
        System.out.println("producto eliminado con el id:" + id);
        return Response.ok().build();
    }
    
}
