# POS BACK END

_Sistema RESTfull en JavaEE, con uso de Glassfish como contenedor de aplicaciones, y configuracion de JTA con Pool de Conexiones
para la comunicacion a la base de datos. Sistema de Base de datos usado: PostgreSQL._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

* [PostgreSQL server 13.4.2, para Windows x64.]
* [PgAdmin 4]
* [JDK 8]
* [IDE(recomendado): Netbeans]
* [Servidor Glassfish 5]


### Base Datos 📋

CREATE TABLE IF NOT EXISTS public.product
(
    id_product integer NOT NULL DEFAULT nextval('product_id_product_seq'::regclass),
    name character varying COLLATE pg_catalog."default",
    description character varying COLLATE pg_catalog."default",
    price character varying COLLATE pg_catalog."default",
    CONSTRAINT product_pkey PRIMARY KEY (id_product)
)


### Instalación 🔧

* [descargar el proyecto]
* [copiar en el directorio deseado]
* [abrir en IDE Netbeans]
* [descargar Glassfish] (https://drive.google.com/file/d/1BwLbvJqQezZ4TnFJqQwqWriWV5WFrpId/view?usp=sharing) - Glassfish con driver de PostgreSQL.
* [Levantar Glassfish]
* [Glassfish: View Domain Admin Console]
* [Crear en Glassfish el pool de conexiones hacia PostgreSQL, y JNDI: jdbc/ProductDb]
* [Hacer Ping en Pool conexiones creado para verificar creacion exitosa.]
* [Clic derecho proyecto: Clean and Build, para descargar las dependencias.]
* [Glassfish, aplicaciones, Crear Deploy y cargar archivo .war del proyecto]
* [Clic derecho proyecto: Run, para ejecutarlo.]


## Ejecutando las pruebas ⚙️

* [Puerto Glassfish default 8080]
* [URL] (http://localhost:8080/pos-edimca/webservice/products) - Obtener listado productos


## Construido con 🛠️

* [Java](https://www.oracle.com/java/technologies/downloads/) - El lenguaje usado
* [PostgreSQL](https://www.postgresql.org/) - Sistema base de datos
* [Netbeans](https://netbeans.apache.org/) - IDE


## Licencia 📄

Este proyecto está bajo la Licencia developer.

## Expresiones de Gratitud 🎁

---
⌨️ con ❤️ por Mauricio Guzman(https://github.com/MauricioHub) 😊
